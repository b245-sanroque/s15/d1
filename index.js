// [SECTION] Comments
// Comments are parts of the code that gets ignored by the language.
// COmments are meant to describe the written code.

// There are two types of comments:
// - Single Line Comments (ctrl + /) denoted by two slashes (//)
/*
	- Multiline Comment (ctrl + shft + /)
	- denoted by / ** /
*/

// [SECTION] Statements and Syntax
	// Statements
		// In programming language, statements are instructions that we tell the computer to perform.
		// JS Statements usually ends with semicolon (;).
			// (;) is also called as delimiter.
		// Semicolons are not required in JS.
			//  it's used to help us train or locate where a statement ends.
	// Syntax
		// In programming, it is the set of rules that describes how statements must be constructed.

console.log("Hello World");

// [SECTION] Variables

// It is used to contain data.

// Declaring a variable
	// tells our devices thate a variable name is created and is ready to store data.

	// Syntax: let/const variableName;

	// let is a keyword that is usually used in declaring a variable.
	let myVariable;
	console.log(myVariable);	//useful for printing values of a variable

	// Initialize a value
		// Storing the initial value of a variable.
		// Assignment Operator (=)
	myVariable = "Hello";

	// const 
		// "const" keyword is used to declare and initialized a constant variable.
		// A "constant" variable does not change.
	// const PI;
	// PI = 3.1416;
	// console.log(PI); //error: Initial value is missing for const.

// Declaring with Initialization
	// a variable is given it's initial/starting value upon declaration.
		// Syntax: let/const variableName = value;

		// let keyword
		let productName = "Desktop Computer"
		console.log(productName);

		let productPrice = 18999;
		// let productPrice = 20000; error: variable has been declared.
		productPrice = 20000;
		console.log(productPrice);

		// const keyword
		const PI = 3.1419
		// PI = 3.14; error: assignment to constant variable.
		console.log(PI);

/*
	Guide in writing variables.

	1. Use the "let" keyword if the variable will contain different values/ can change over time.
		Naming Convention: 
			- variable name should start with "lowercase characters" and "camelCasing" is used for multiple word
	2. Use the "const" if the variable does not change.
		Naming Convention:
			- "const" keyword should be followed by ALL CAPITAL VARIABLE NAME (single value variable)
	3. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

	Avoid this one: let word = "John Smith"
*/

// Multiple Variable Declaration

		let productCode = "DC017", productBrand = "Dell";
		console.log(productCode, productBrand);

// Using a reserved keyword

		// const let = "hello";
		// console.log(let); error: lexically bound is disallowed.

// [SECTION] Data Types

// In JavaScript, there are six types of data

	// String,
		// are series of characters that create a word, a phrase, a sentence or anything related to creating text.
		// Strings in JavaScript can be written using a single quote ('') or double quote ("")

		let country = "Phillipines";
		let province = "Metro Manila";

		// Concatenating Strings
		// Multiple string values that can be combined to create a single String using the "+" symbol.
		// e.g. "I live in Metro Manila, Philippines"
		let greeting = "I live in " + province + "," + country
		console.log(greeting);

	// Escape Characters,
	// (\) in string combination with other characters can produce different result
		// "\n" refers to creating a new line.
		let mailAddress = "Quezon City\nPhillipines"
		console.log(mailAddress);

		// Expected output: John's employees went home early.
		let message = "John's employees went home early.";
		console.log(message);

		// using the escape characters (\)
		message = 'Jane\'s employees went home early.';
		console.log(message);

	// Numbers
		// includes positive, negative, or numbers with decimal places.
		// Integers/Whole Numbers
		let headcount = 26;
		console.log(headcount);

		// Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);

		// Exponential Notation
		let planetDistance = 2e10;
		console.log(planetDistance);

		// Combine numbers and strings
		console.log("John's grade last quarter is "+grade);

	// Boolean
		// Boolean values are normally used to store values relating to the state of certain things.
		// true or false (two logical values)

		let isMarried = false;
		let isGoodConduct = true;

		console.log("isMarried: "+isMarried);
		console.log("isGoodConduct: "+isGoodConduct);

	// Arrays ([ ])
		// are special kind of data type that can be used to store multiple related values.
		// Syntax: let/const arrayName = [elementA, elementB, ... elementNth];

		let grades = [98.7, 92.1, 90.2, 94.6];
		console.log(grades);

		// constant variable cannot be reassigned
		// grades = 100; //not allowed
		// Changing the elements of an array or changing the properties of an object is allowed in constant variable.
		grades[0] = 100; //reference/index value
		console.log(grades);

		// this will work but not recommended (they are unrelated to each other).
		let details = ["John", "Smith", 32, true];
		console.log(details);

	// Objects 
		// Objects are another special kind of data  type that is used to mimic real world objects/items.
		// used to create complex data that contains information relevant to each other.
		/*
			Syntax:
				let/const ogjectName = {
					propertyA: valueA,
					propertyB: valueB
				}
		*/
		let person = {
			fullName: "Juan Dela Cruz",
			age: 35,
			isMarried: false,
			contact: ["+63912 345 6789", "8123 456"],
			address: {
				houseNumber: "345",
				city: "manila"
			}
		};

		 console.log(person);

		 // They're also useful for creating abstract objects
		 let myGrades = {
		 	firstGrading: 98.7,
		 	secondGrading: 92.1,
		 	thirdGrading: 90.2,
		 	fourthGrading: 94.6
		 }

		 console.log(myGrades);

		  // typeof operator is used to determine the type of data or the value of a variable.

		 console.log(typeof myGrades); //object

		 // arrays is a special type of object with methods.
		 console.log(typeof ragdes); //object

 		// Null
		 	// indicates the absence of a value
		 	let spouse = null;
		 	console.log(spouse);

		 // Undefined 
		 	// indicates tha a variable has not been given a value yet.
		 	let fullName;
		 	console.log(fullName);